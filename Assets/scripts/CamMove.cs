﻿using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using System;
using Unity.VisualScripting;

public class CamMove : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
	public Sprite[] numbers;
	public GameObject numbprefab, bulletPref,endingPoint,startingPoint;
	public Camera cam;
	public Sprite[] stateSprites;
	public Point[] points;
	Ray _ray;
	RaycastHit _hit;
	void Awake() => Application.targetFrameRate = 30;
	public IEnumerator TextControl(Transform text, float number, string str) {
		string numberString = Math.Abs(number).ToString();
		if (numberString.Length == 1) numberString = 0+numberString;
		for (int i = 0; i < numberString.Length; i++) text.transform.GetChild(0).GetChild(i).GetComponent<SpriteRenderer>().sprite = numbers[int.Parse(numberString[i].ToString())];
		if (numberString.Length == 2) {
			text.transform.GetChild(0).GetChild(0).localPosition = new Vector3(.35f, 0, 0);
			text.transform.GetChild(0).GetChild(1).localPosition = new Vector3(1.05f, 0, 0);
			text.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
		}
		else if(numberString.Length == 3) {
			text.transform.GetChild(0).GetChild(0).localPosition = new Vector3(0,0,0);
			text.transform.GetChild(0).GetChild(1).localPosition = new Vector3(.7f,0,0);
			text.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
			text.transform.GetChild(0).GetChild(2).localPosition = new Vector3(1.4f,0,0);
		}
		yield return null;
	}
	public void OnPointerUp(PointerEventData eventData) {
		_ray = cam.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(_ray, out _hit, 100f))
			if (_hit.collider != null) { 
				GameObject NewPoint = _hit.collider.gameObject;
				if (NewPoint == null) return;
				if (endingPoint != null) {
					if (endingPoint.GetComponent<Point>().state != state.player) return;
					if (NewPoint == endingPoint&& endingPoint.transform.GetChild(1).gameObject.activeSelf) { 
						endingPoint?.transform.GetChild(1).gameObject.SetActive(false);
						startingPoint?.transform.GetChild(1).gameObject.SetActive(false);
						endingPoint = null;
						startingPoint = null;
						return;
					}
				}
				if (endingPoint==null)
					if (NewPoint.GetComponent<Point>().state==state.player) {
						endingPoint = NewPoint;
						startingPoint = null;
					}
				if (endingPoint != null) {
					endingPoint.transform.GetChild(1).gameObject.SetActive(true);
					startingPoint = endingPoint;
					endingPoint= NewPoint;
				}
				if ((startingPoint != null&&endingPoint!=null)&&(startingPoint!=endingPoint)) {
					bool have = false;
					for (int i = 0; i < endingPoint.GetComponent<Point>().lines.linesId.Count; i++) {
						if (endingPoint.GetComponent<Point>().lines.linesId[i].idd == int.Parse(startingPoint.name.Split(' ')[1])) {
							have = true;
							Bullet go = Instantiate(bulletPref, startingPoint.transform.position, Quaternion.identity).GetComponent<Bullet>();
							if (go == null) return;
							Point endingp = endingPoint.GetComponent<Point>();
							Point startingp = startingPoint.GetComponent<Point>();
							go.target = endingPoint;
							go.endState = state.player;
							go.startState = state.player;
							go.spriteId = endingp.sprite_id;
							//////////////////////////////////
							//print("start/end");
							//print(startingp.points);
							//print(endingp.points);
							//////////////////////////////////
							int addingp = 0;
							if (endingp.state == state.nikt) addingp = endingp.points;
							if (endingp.state == state.player) addingp = -endingp.points;
							int n = (int)char.ToUpper((char)endingp.pointLvl) - 1;
							if (n < 0) n = 0;
                            //////////////////////////////////
                            //print("1/2/3");
                            //print(endingp.lvls[n].maxPointsInPlanet);
                            //print(startingp.points);
                            //print(addingp);
                            //////////////////////////////////
                            if (endingp.lvls[n].maxPointsInPlanet >= startingp.points-addingp){
								go.minusPoints = startingp.points;
								startingp.points = 0;
                            }
							else {
								go.minusPoints = endingp.lvls[n].maxPointsInPlanet + addingp;
                                startingp.points = startingp.points - (endingp.lvls[n].maxPointsInPlanet+addingp);
                            }
                            if (go?.transform?.GetChild(1)?.GetChild(0)?.GetComponent<Text>())
								go.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = go.minusPoints.ToString();
							go.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = go.sprites[(int)go.startState-2];
							endingPoint.transform.GetChild(1).gameObject.SetActive(false);
							startingPoint.transform.GetChild(1).gameObject.SetActive(false);
							startingPoint = null;
							endingPoint = null;
							break;
						}
					}
					if (have == false){
						endingPoint.transform.GetChild(1).gameObject.SetActive(false);
						startingPoint.transform.GetChild(1).gameObject.SetActive(false);
						startingPoint = null;
						endingPoint = null;
					}
				}
			}
	}
	public void OnPointerDown(PointerEventData eventData) { }
	public void SetStatekDatas(Bullet go, Point p,int i) {
		go.target = points[p.lines.linesId[i].idd].transform.gameObject;
		go.minusPoints = p.points;
        go.endState = state.enemy;
		go.startState = p.state;
		go.spriteId = go.target.GetComponent<Point>().sprite_id;
		go.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = go.minusPoints.ToString();
		go.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = go.sprites[(int)go.startState];
		p.points = 0;
	}
	public void ChangeTimeSpeed(float speed)=>Time.timeScale = speed;
}
