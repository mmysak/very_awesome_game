using UnityEngine;
using UnityEngine.UI;

public class TutorialButton : MonoBehaviour
{
    public Image tutorialImage; // Obrazek, kt�ry b�dzie wy�wietlany jako poradnik
    public Sprite[] tutorialFrames; // Tablica z klatkami poradnika
    private int currentFrameIndex = 0;
    public Button tutorialButtom_in;
    private void Start()
    {
        tutorialButtom_in.gameObject.SetActive(false);
        // Inicjalizacja obrazu poradnika
        if (tutorialImage != null && tutorialFrames.Length > 0)
        {
            tutorialImage.sprite = tutorialFrames[currentFrameIndex];
        }
    }

    public void ShowNextFrame()
    {
        // Wy�wietl nast�pn� klatk� poradnika
        currentFrameIndex = (currentFrameIndex + 1) % tutorialFrames.Length;
        tutorialButtom_in.gameObject.SetActive((currentFrameIndex==0?false:true));
        if (tutorialImage != null)
        {
            tutorialImage.sprite = tutorialFrames[currentFrameIndex];
        }
    }
}