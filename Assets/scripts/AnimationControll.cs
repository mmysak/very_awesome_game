using UnityEngine;

public class AnimationControll: MonoBehaviour
{
	public enum Team : int { enemy, neutral, player }
	public enum Size : int { small, medium, large };

	[SerializeField] private MeshRenderer planet;
	[SerializeField] private MeshRenderer ring;
	[SerializeField] private Material[] planets;
	[SerializeField] private Material[] rings;
	[SerializeField] private Size size;
	[SerializeField] private Team team;
	private Animator anim;
	void Start()=>anim = GetComponent<Animator>();
	void Update() {
		ChangeSize(size);
		ChangeTeam(team);
	}

	public void ChangeSize(Size newSize) {
		size = newSize;

		switch (size) {
			case Size.small:
				anim.SetFloat("Speed", 0.1f);
				break;
			case Size.medium:
				anim.SetFloat("Speed", 0.1f);
				break;
			case Size.large:
				anim.SetFloat("Speed", 0.1f);
				break;
		}
	}

	public void ChangeTeam(Team newTeam) {
		team = newTeam;
		planet.material = planets[(int)size * 3 + (int)team];
		if (ring) ring.material = planets[(int)size * 3 + (int)team];
        if (!ring) return;
		if ((int)size == 2) ring.material = rings[(int)team];
	}
}
